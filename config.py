import yaml
import argparse
from pathlib import Path
import torch


def config():
    """
    Reads the command switches and creates a config
    Command line switches override config files
    :return:
    """

    """ config """
    parser = argparse.ArgumentParser(description='load_yaml_file')
    parser.add_argument('-r', '--run_id', type=int, default=-1)
    parser.add_argument('--comment', type=str)
    parser.add_argument('--demo', action='store_true', default=False)
    parser.add_argument('-l', '--load', type=str, default=None)
    parser.add_argument('--transfer_load', type=str, default=None)
    parser.add_argument('--checkpoint_freq', type=int)
    parser.add_argument('--config', type=str, default=None)
    parser.add_argument('--processes', type=int)
    parser.add_argument('--seed', type=int, default=None)

    """cma params"""
    parser.add_argument('--cma_algo', type=str)
    parser.add_argument('--cma_step_mode', type=str)
    parser.add_argument('--cma_step_decay', type=float, default=None)
    parser.add_argument('--cma_initial_step_size', type=float, default=None)
    parser.add_argument('--cma_samples', type=int, default=None)
    parser.add_argument('--cma_oversample', type=int)

    """ visualization params """
    parser.add_argument('--display', action='store_true')
    parser.add_argument('--display_freq', type=int)
    parser.add_argument('--display_kp_rows', type=int)

    """ model parameters """
    parser.add_argument('--opt_level', type=str)
    parser.add_argument('--model_type', type=str)
    parser.add_argument('--model_in_channels', type=int)
    parser.add_argument('--model_keypoints', type=int)
    parser.add_argument('--transporter_combine_mode', type=str)

    """ policy parameters """
    parser.add_argument('--policy_action_select_mode', type=str)
    parser.add_argument('--policy_depth', type=int)

    """ gym env parameters """
    parser.add_argument('--gym_reward_count_limit', type=int, default=None)

    """ hyper-parameters """
    parser.add_argument('--optimizer', type=str, default='Adam')

    """ data and data augmentation parameters """
    parser.add_argument('--dataset', type=str)
    parser.add_argument('--dataset_train_len', type=int)
    parser.add_argument('--dataset_test_len', type=int)
    parser.add_argument('--dataset_randomize', type=int)

    parser.add_argument('--data_aug_tps_cntl_pts', type=int)
    parser.add_argument('--data_aug_tps_variance', type=float)
    parser.add_argument('--data_aug_max_rotate', type=float)
    
    parser.add_argument('--lr', type=float, default=1e-4, help='')  #
    parser.add_argument('--max_epochs', type=int, default=50, help='') #
    parser.add_argument('--sample_rate', type=int, default=4, help='') #
    parser.add_argument('--batch_size', type=int, default=32, help='') #
    parser.add_argument('--num_workers', type=int, default=1, help='') #
#parser.add_argument('--metric', type=str, default='mse', help='') 
    parser.add_argument('--name', type=str, default='out', help='') #
    parser.add_argument('--data_root', type=str, default='UltrasoundVideoSummarization/', help='') #
    parser.add_argument('--LUS_num_chan', type=int, default=10, help='') #
    parser.add_argument('--LUS_num_keypoints', type=int, default=10, help='') #
#parser.add_argument('--vq_path', type=str, default='VQVAE_unnorm_trained.pth', help='')
    parser.add_argument('--htmaplam', type=float, default=0.1, help='') #
    parser.add_argument('--device', type=str, default='cuda:0', help='') 

    args = parser.parse_args(args=[])

    def load_if_not_set(filepath, args):
        """
        Adds the update to args if it's not loaded
        :param filepath:
        :return:
        """
        with Path(filepath).open() as f:
            conf = yaml.load(f, Loader=yaml.FullLoader)
            for key, value in conf.items():
                if key in vars(args) and vars(args)[key] is None:
                    vars(args)[key] = conf[key]
                elif key not in vars(args):
                    vars(args)[key] = conf[key]
        return args

    if args.config is not None:
        args = load_if_not_set(args.config, args)

    args = load_if_not_set('configs/defaults.yaml', args)

    if args.device is None:
        args.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    else:
        args.device = torch.device(args.device)

    def counter():
        run_id_pid = Path('./.run_id')
        count = 1
        if run_id_pid.exists():
            with run_id_pid.open('r+') as f:
                last_id = int(f.readline())
                last_id += 1
                count = last_id
                f.seek(0)
                f.write(str(last_id))
        else:
            with run_id_pid.open('w+') as f:
                f.write(str(count))
        return count

    if args.run_id == -1:
        args.run_id = counter()

    return args
